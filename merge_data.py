import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt


def merge(folder, cols, join_col="DATE", join_method="outer", new_col_name="STATION"):
    # create dateframe for store concat cols
    concat = pd.DataFrame({join_col: []})

    files = os.listdir(folder)

    for f in files:
        df = pd.read_csv(folder + f)
        station_id = df["STATION"][0]
        station_name = df["NAME"][0]

        print(f"{station_id} \t {station_name}")

        # create columns if not exist
        for col in cols:
            if col not in df:
                df[col] = np.nan

        start_date = "1970-01-01"
        end_date = "2010-01-01"

        # ตรวจสอบจำนวนวัน ว่าเยอะพอไหม ถ้าเยอะพอก็ให้เก็บข้อมูลไว้
        if len(df[(df['DATE'] >= start_date) & (df['DATE'] <= end_date)]) >= 5000:
            # merge columns
            concat = pd.merge_ordered(concat, df[cols], on=join_col, how=join_method)
            # rename last column
            concat = concat.rename(columns={concat.columns[-1]: station_id})
        else:
            print(f"Not enough data: {len(df[(df['DATE'] >= start_date) & (df['DATE'] <= end_date)])}")

    return concat


if __name__ == "__main__":

    sep = ","
    folders = ["./ghcnd_rename/japan/", "./ghcnd_rename/uk/"]
    output_files = [
        [
            "./clean/japan/TAVG.csv",
            "./clean/japan/TMAX.csv",
            "./clean/japan/TMIN.csv",
            "./clean/japan/PRCP.csv",
        ],
        [
            "./clean/uk/TAVG.csv",
            "./clean/uk/TMAX.csv",
            "./clean/uk/TMIN.csv",
            "./clean/uk/PRCP.csv",
        ],
    ]

    indices = [["DATE", "TAVG"], ["DATE", "TMAX"], ["DATE", "TMIN"], ["DATE", "PRCP"]]

    for k in range(len(folders)):
        for i in range(len(indices)):
            concat = merge(
                folders[k],
                indices[i],
                join_col=indices[i][0],
                join_method="outer",
                new_col_name="STATION",
            )

            # split date column
            # concat = concat[(concat['DATE'] >= '1970-01-01') & (concat['DATE'] <= '2005-12-31')]
            # concat['DATE'] = pd.to_datetime(concat['DATE'])
            # concat['A_year'] = concat['DATE'].dt.year
            # concat['B_month'] = concat['DATE'].dt.month
            # concat['C_day'] = concat['DATE'].dt.day
            # concat = concat[sorted(concat.columns)]

            # concat = concat.rename(columns={
            #     'A_year': 'year',
            #     'B_month': 'month',
            #     'C_day': 'day'
            # })
            # concat = concat.drop(['DATE'], axis=1)

            concat.to_csv(output_files[k][i], index=False, sep=sep)
