import pandas as pd
import numpy as np
import os

def select_col(df, columns=['STATION', 'NAME', 'LATITUDE', 'LONGITUDE', 'ELEVATION', 'DATE', 'PRCP', 'TAVG', 'TMAX', 'TMIN']):
    # create columns if not exist
    for col in columns:
        if col not in df:
            df[col] = np.nan
    
    return df[columns]


if __name__ == "__main__":
    folder = "./GHCND_csv/UK/"
    files = os.listdir(folder)[1:]

    for f in files:
        df = pd.read_csv(folder + f)
        columns = ['STATION', 'NAME', 'LATITUDE', 'LONGITUDE', 'ELEVATION', 'DATE', 'PRCP', 'TAVG', 'TMAX', 'TMIN']
        climate_col = ['PRCP', 'TAVG', 'TMAX', 'TMIN']
        df = select_col(df, columns)
        df[climate_col] /= 10

        station_id = df.STATION[0]
        station_name = df.NAME[0].replace(',', '').replace(' ', '_')
        print(f"{station_id} \t {station_name}")
        file_name = f"{station_name}_{station_id}.csv"
        output_folder = "./ghcnd_rename/uk/"
        df.to_csv(f"{output_folder}{file_name}", index=False)