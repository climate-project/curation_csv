# Curation CSV

จัดระเบียบ CSV สำหรับให้ อ.

## structure

```
/clean (เก็บ csv ที่จัดเรียบร้อย)
    |- /country_name (เก็บข้อมูลประเทศที่จัดระเบียบแล้ว)
        |- PRCP, TAVG, TMAX, TMIN .csv
    |- station_country_name.csv (เก็บรายชื่อสถานี)

# raw csv
/GHCND_csv
    |- ข้อมูลจาก GHCND ยังไม่ได้จัดชื่อไฟล์
    |- THxxx.csv
/GHCND_rename
    |- ข้อมูลจาก GHCND จัดชื่อไฟล์แล้ว
    |- station_name_station_id.csv
/observed_csv
    |- ข้อมูลจาก climate.gov (ไม่ใช้แล้ว ใช้จาก GHCND)
    
rename_select_col.py
- แก้ไขชื่อไฟล์ให้เป็น ชื่อสถานี_รหัสสถานี
- เลือก column ที่ต้องการ (สำคัญๆ ได้แก่ PRCP TAVG TMAX TMIN)
- ต้องหารด้วย 10 เพราะข้อมูลเก็บในรูปแบบที่คูณ 10 มา
- ถ้าไม่มี ให้เติมด้วย nan

merge_data.py
- รวมข้อมูล 
- ตรวจสอบจำนวนวันว่าครบตามที่ต้องการไหม เพราะว่าบางสถานี อาจจะบันทึกในช่วงที่ต้องการไม่เยอะ (หยุดบันทึกไปก่อน)
- ตรวจสอบจากวัน (เพราะจากข้อมูลจะมี null)
- รวมข้อมูลเป็น columns = เลข id สถานี กับ วันที่
- ชื่อไฟล์เป็น TAVG, PRCP ....

merge_station.py
- รวมรายชื่อสถานี
- ตรวจสอบจำนวนวัน

```

## สรุปวิธีใช้
- เปลี่ยนชื่อไฟล์ก่อน
- รวมข้อมูลและสถานี

## หมายเหตุ

- ข้อมูลจาก climate คือ [climate.gov](https://www.climate.gov/maps-data/dataset/past-weather-zip-code-data-table)

- ข้อมูลจาก GHCND คือ [NCDC NOAA](https://www.ncdc.noaa.gov/ghcnd-data-access?)

- GHCND raw เอาไว้ไปสร้าง GHCNDEX (ที่เป็น index)

- climate.gov และ noaa มีสถานีไม่เท่ากัน และไม่เหมือนกัน

- observed_csv หาได้จาก repository `climate-simple-plot/station_data/`
