import pandas as pd
import numpy as np
import os


def merge(folder, cols, sort_col):
    files = os.listdir(folder)
    df_list = []
    for f in files:
        df = pd.read_csv(folder + f)[cols]
        print(df[sort_col][0])

        start_date = "1970-01-01"
        end_date = "2010-01-01"
        xx = pd.read_csv(folder + f)

        # ตรวจสอบจำนวนวัน ว่าเยอะพอไหม ถ้าเยอะพอก็ให้เก็บ station ไว้
        if len(xx[(xx['DATE'] >= start_date) & (xx['DATE'] <= end_date)]) >= 5000:
            df_list.append(df.drop_duplicates())
        else:
            print("Not Enough data")

    concat = pd.concat(df_list)
    return concat.sort_values(by=sort_col)


if __name__ == "__main__":
    sep = ","
    folders = ["./ghcnd_rename/japan/", "./ghcnd_rename/uk/"]
    output_files = ["./clean/station_japan.csv", "./clean/station_uk.csv"]

    cols = ["STATION", "NAME", "LATITUDE", "LONGITUDE"]
    sort_col = "STATION"

    for i in range(len(folders)):
        concat = merge(folders[i], cols, sort_col)
        concat[cols[2:]] = np.round(concat[cols[2:]], 3)
        concat.to_csv(output_files[i], index=False, sep=sep)
